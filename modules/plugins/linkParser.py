#!/usr/bin/python2
# encoding: utf-8

from urllib2 import urlopen
from urlparse import urlparse
from random import choice
import re

from ..config import cfg
from ..irc import notice, reply
from ..pluginManager import register_function

url_regex = re.compile(r"""(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))""") #get links from messages
content_regex = re.compile("Content-Type: text/.?html", re.IGNORECASE|re.DOTALL) #do not download images/videos etc
title_regex = re.compile('<title>(.*?)</title>', re.IGNORECASE|re.DOTALL) #get the title from html dom


def on_init():
	global linksCache
	
	try:
		f = open("modules/plugins/linksCache.txt", 'r')
		linksCache = f.readlines()
		f.close()
	except:
		linksCache = []

def on_message(ident, nick, channel, msgtup):
	if channel == cfg['HOME']:
		for url in [match[0] for match in url_regex.findall(" ".join(msgtup))]:
			try:
				hostname = urlparse(url).hostname
				html_reply = urlopen(url, timeout=3)
				html_info = str(html_reply.info())
				if content_regex.search(html_info):
					html_string = html_reply.read()
					title_match = title_regex.search(html_string)
					if title_match:
						title = title_match.group(1)
						linkinfo = "02^{0}: {1}".format(hostname, title)
						notice(linkinfo)
						linkinfo = "02^ {0} : {1}".format(url, title)
						if linkinfo not in linksCache:
							linksCache.insert(0, linkinfo)
							if len(linksCache) > 500:
								linksCache.pop(500)
				html_reply.close()
			except Exception,e:
				print(str(e))
		
		#!findlink		
		if msgtup[0] == "!findlink":
			request = " ".join(msgtup[1:len(msgtup)])
			for linkinfo in linksCache:
				if re.search(request, linkinfo, re.IGNORECASE):
					notice(linkinfo)
					return
			reply(nick, "No links found.")

		#!randomlink
		elif msgtup[0] == "!randomlink":
			if len(linksCache) > 0:
				notice(choice(linksCache))
			else:
				reply(nick, "No links recorded.")

def on_terminate():
	f = open("modules/plugins/linksCache.txt", 'w')
	f.write("\r\n".join(linksCache))
	f.close()

register_function('on_init', on_init)
register_function('on_message', on_message)
register_function('on_terminate', on_terminate)
