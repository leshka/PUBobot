#!/usr/bin/python2
# encoding: utf-8
import importlib

import console
#from plugins import linkParser

class Event():
	def __init__(self):
		self.functions = []
		
	def on(self, func):
		self.functions.append(func)

	def off(self, func):
		if func in self.functions:
			self.functions.remove(func)
		else:
			raise("This function is not registered on this event.")

	def trigger(self, args):
		for func in self.functions:
			func(*args)

def init():
	global plugins, events
	#init event system
	events = dict()
	events['on_think'] = Event()
	events['on_init'] = Event()
	events['on_message'] = Event()
	events['on_private_message'] = Event()
	events['on_nick_change'] = Event()
	events['on_join'] = Event()
	events['on_part'] = Event()
	events['on_quit'] = Event()
	events['on_kick'] = Event()
	events['on_ban'] = Event()
	events['on_mode_change'] = Event()
	events['on_disconnect'] = Event()
	events['after_connect'] = Event()
	events['on_terminate'] = Event()
		
	#load plugins
	plugins = []
	try:
		f = open('plugins.cfg', 'r')
	except Exception,e:
		console.display("ERROR OPPENING FILE 'plugins.cfg'. Exception: {0}".format(str(e)))
		return
	
	for line in f.readlines():
		try:
			line = line.strip()
			if len(line) > 1:
				if line[0] != "#":
					module_obj = importlib.import_module('modules.plugins.'+line)
					module_obj.on_init()
					plugins.append(module_obj)
					console.display("Plugin '{0}' sucsessfully loaded!".format(line))
		except Exception,e:
			console.display("ERROR IMPORTING PLUGIN @ '{0}'. Exception: {1}".format(line, str(e)))

def register_function(event_name, func):
	if event_name in events.keys():
		events[event_name].on(func)
	else:
		raise("Event {0} not found.".format(event_name))

def unregister_function(event_name, func):
	if event_name in events.keys():
		events[event_name].off(func)
	else:
		raise("Event {0} not found.".format(event_name))

def trigger_event(event_name, args):
	if event_name != "on_think":
		print(event_name, args)
	events[event_name].trigger(args)
