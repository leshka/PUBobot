#!/usr/bin/python2
# encoding: utf-8
import os
import time

#my modules
from modules import console, config, stats2, bot, irc, scheduler, pluginManager

#start the bot
config.init()
scheduler.init()
stats2.init()
bot.init()
console.init()
pluginManager.init()
irc.init()

os.environ['TZ'] = config.cfg['TIMEZONE']
time.tzset()

while 1:
	frametime = time.time()
	
	irc.run(frametime)
	irc.send(frametime)
	scheduler.run(frametime)
	console.run()
	pluginManager.trigger_event("on_think", (frametime,))
	time.sleep(0.5)
